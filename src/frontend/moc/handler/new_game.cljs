(ns moc.handler.new-game
  (:require [re-frame.core :refer [dispatch register-handler]]
            [moc.ajax :as ajax]
            [moc.router :refer [navigate!]]
            [moc.validate.util :refer [validate]]
            [moc.validate.game :as game.validate]))

;;; FORM STATE

(register-handler
 :game.new/set-name
 (fn [db [_ value]]
   (assoc-in db [:ui :game/new :name] value)))

(register-handler
 :game.new/set-template
 (fn [db [_ value]]
   (assoc-in db [:ui :game/new :template] value)))

(register-handler
 :game.new/set-time
 (fn [db [_ value]]
   (assoc-in db [:ui :game/new :time] value)))

(register-handler
 :game.new/set-location-name
 (fn [db [_ value]]
   (assoc-in db [:ui :game/new :location-name] value)))

(register-handler
 :game.new/reset-state
 (fn [db _]
   (assoc-in db [:ui :game/new] {:name ""
                                 :template -1
                                 :time "17.02.1925 15:30"
                                 :location-name "Arkham, Massachusetts"
                                 :errors {}})))

(register-handler
 :game.new/set-errors
 (fn [db [_ errors]]
   (assoc-in db [:ui :game/new :errors] errors)))

(register-handler
 :game.new/reset-errors
 (fn [db _]
   (assoc-in db [:ui :game/new :errors] {})))

;;; SERVER COMMUNICATION

(register-handler
 :game.new/send
 (fn [db [_ data]]
   (let [errors (validate data game.validate/new-game-schema)]
     (if errors
       (dispatch [:game.new/set-errors errors])
       (do
         (dispatch [:game.new/reset-errors])
         (ajax/request {:path [:api.game/new]
                        :data data
                        :on-success (fn [{:keys [id]}]
                                      (navigate! [:url.game/settings :id id]))
                        :on-fail #(dispatch [:game.new/set-errors %])}))))
   db))
