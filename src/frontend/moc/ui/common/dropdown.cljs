(ns moc.ui.common.dropdown)

(defn dropdown [{:keys [label disabled? value error on-change]} options]
  [:div.dropdown
   [:label
    label
    [:select {:disabled disabled?
              :value value
              :on-change (or on-change (fn [_] nil))}
     (doall (for [[value label] options]
              [:option {:key value
                        :value value}
               label]))]]
   (when error
     [:div.error
      [:i.fa.fa-warning]
      [:span.text error]])])
