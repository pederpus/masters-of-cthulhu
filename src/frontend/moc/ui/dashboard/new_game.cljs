(ns moc.ui.dashboard.new-game
  (:require [re-frame.core :refer [dispatch dispatch-sync subscribe]]
            [moc.ui.common.input :refer [input]]
            [moc.ui.common.checkbox :refer [checkbox]]
            [moc.ui.common.dropdown :refer [dropdown]]
            [moc.ui.common.button :refer [button]]
            [moc.ui.common.handlers :refer [dispatch-on-enter pass-to-dispatch]]))

(defn new-game-header []
  [:span.title "New game"])

(defn new-game [_]
  (dispatch-sync [:game.new/reset-state])
  (let [loading? (subscribe [:loading?])
        state (subscribe [:game.new/form-state])
        errors (subscribe [:game.new/form-errors])]
    (fn [_]
      [:div {:on-key-up #(dispatch-on-enter % [:game.new/send @state])}
       [input {:label "Name"
               :auto-focus true
               :placeholder "Rats in the walls"
               :value (:name @state)
               :error (:name @errors)
               :on-change #(pass-to-dispatch % :game.new/set-name)}]

       [dropdown {:label "Template"
                  :value (:template @state)
                  :error (:template @errors)
                  :on-change #(pass-to-dispatch % :game.new/set-template)}
        [[-1 "None"]]]

       [input {:label "Time"
               :value (:time @state)
               :error (:time @errors)
               :on-change #(pass-to-dispatch % :game.new/set-time)}]

       [input {:label "Location"
               :value (:location-name @state)
               :error (:location-name @errors)
               :on-change #(pass-to-dispatch % :game.new/set-location-name)}]

       [button {:loading? @loading?
                :on-click #(dispatch [:game.new/send @state])}
        "Create"]])))
