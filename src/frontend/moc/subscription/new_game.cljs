(ns moc.subscription.new-game
  (:require [reagent.core :as reagent]
            [re-frame.core :refer [register-sub]])
  (:require-macros [reagent.ratom :refer [reaction]]))

(register-sub
 :game.new/form-state
 (fn [db _]
   (reaction (select-keys (-> @db :ui :game/new) [:name
                                                  :template
                                                  :time
                                                  :location-name]))))

(register-sub
 :game.new/form-errors
 (fn [db _]
   (reaction (-> @db :ui :game/new :errors))))
