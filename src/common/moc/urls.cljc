(ns moc.urls)

(def urls ["/" {"" :url.dashboard/index
                "auth" {"/login" :url.auth/login
                        "/register" :url.auth/register
                        ["/token/" :token] :url.auth/token}
                "dashboard" {"/profile" :url.dashboard/profile
                             "/game" :url.dashboard/new-game}
                ["game/" :id] {"/settings" :url.game/settings}

                "api" {"/auth" {"/login" :api.auth/login
                                "/register" :api.auth/register
                                "/logout" :api.auth/logout}
                       "/user" {"/me" :api.user/me
                                "/profile" :api.user/profile}
                       "/game" {"/new" :api.game/new}}}])
