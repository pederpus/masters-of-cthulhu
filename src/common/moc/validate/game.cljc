(ns moc.validate.game
  (:require [bouncer.validators :as v]
            [moc.validate.util :as util]))

(def new-game-schema
  {:name (into [v/required] util/name-validator)
   :template [v/required [v/in-range [-1 2147483647]]]
   :time [v/required [v/datetime "DD.mm.YYYY HH:mm"]]
   :location-name (into [v/required] util/name-validator)})
