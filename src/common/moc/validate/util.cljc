(ns moc.validate.util
  (:require [bouncer.core :as bouncer]
            [bouncer.validators :as v]))

(def name-validator
  [v/string
   [v/min-count 2 :message "Name must at least contain two letters"]
   [v/max-count 50 :message "Name cannot contain more than fifty letters"]])

(defn- format-errors [errors]
  (when errors
    (-> (fn [acc k v]
          (assoc acc k (first v)))
        (reduce-kv {} errors))))

(defn validate [item schema]
  (-> (bouncer/validate item schema)
      second
      :bouncer.core/errors
      format-errors))
